﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace DNScriptWFM
{
    public static partial class WindowsApi
    {
        #region Mouse_Event
        public static void MouseLeftClickEvent(int dx, int dy, int data)
        {
            SetCursorPos(dx, dy);
            System.Threading.Thread.Sleep(2 * 1000);
            mouse_event(MouseEventFlag.LeftDown | MouseEventFlag.LeftUp, dx, dy, data, UIntPtr.Zero);
        }
       
        public static void SendMouse(params MouseAction[] actions)
        {
            List<Input> pendingInputs = new List<Input>();
            foreach (var act in actions)
            {
                if (act is MouseDown || act is MouseUp)
                {
                    pendingInputs.Add(new Input
                    {
                        type = (uint)InputType.Mouse,
                        u = new InputUnion
                        {
                            mi = new MouseInput
                            {
                                dx = 0,
                                dy = 0,
                                mouseData = 0,
                                dwFlags = (uint)act.GetFlag(),
                                time = 0,
                                dwExtraInfo = IntPtr.Zero,
                            }
                        }
                    });
                }
                if (act is MouseWheel)
                {
                    pendingInputs.Add(new Input
                    {
                        type = (uint)InputType.Mouse,
                        u = new InputUnion
                        {
                            mi = new MouseInput
                            {
                                dx = 0,
                                dy = 0,
                                mouseData = act.GetMouseData(),
                                dwFlags = (uint)act.GetFlag(),
                                time = 0,
                                dwExtraInfo = IntPtr.Zero,
                            }
                        }
                    });
                }
            }
            var pendingInputsArr = pendingInputs.ToArray();

            var gch = GCHandle.Alloc(pendingInputsArr, GCHandleType.Pinned);
            SendInput((uint)pendingInputsArr.Length, gch.AddrOfPinnedObject(), Marshal.SizeOf(typeof(Input)));

            gch.Free();
        }



        public interface MouseAction
        {
            WindowsApi.MouseEventFlag GetFlag();
            int GetMouseData();
        }

        public class MouseDown : MouseAction
        {
            bool isRightBtn = false;
            public MouseDown(bool rightBtn)
            {
                isRightBtn = rightBtn;
            }
            MouseEventFlag MouseAction.GetFlag()
            {
                if (isRightBtn) return MouseEventFlag.RightDown;
                return MouseEventFlag.LeftDown;
            }

            int MouseAction.GetMouseData()
            {
                throw new InvalidOperationException();
            }
        }

        public class MouseUp : MouseAction
        {
            bool isRightBtn = false;
            public MouseUp(bool rightBtn)
            {
                isRightBtn = rightBtn;
            }
            MouseEventFlag MouseAction.GetFlag()
            {
                if (isRightBtn) return MouseEventFlag.RightUp;
                return MouseEventFlag.LeftUp;
            }

            int MouseAction.GetMouseData()
            {
                throw new InvalidOperationException();
            }
        }

        public class MouseWheel : MouseAction
        {
            int _ticks = 0;
            public MouseWheel(int ticks)
            {
                _ticks = ticks;
            }

            MouseEventFlag MouseAction.GetFlag()
            {
                return MouseEventFlag.Wheel;
            }

            int MouseAction.GetMouseData()
            {
                return _ticks * 120;
            }
        }
        #endregion
        #region Keybd_Event
        public static void SendKey(DirectXKeyStrokes key, bool KeyUp)
        {
            uint flagtosend;
            if (KeyUp)
            {
                flagtosend = (uint)(KeyEventF.KeyUp | KeyEventF.Scancode);
            }
            else
            {
                flagtosend = (uint)(KeyEventF.KeyDown | KeyEventF.Scancode);
            }

            Input[] inputs =
            {
                new Input
                {
                    type = (uint) InputType.Keyboard,
                    u = new InputUnion
                    {
                        ki = new KeyboardInput
                        {
                            wVk = 0,
                            wScan = (ushort) key,
                            dwFlags = flagtosend,
                            dwExtraInfo = GetMessageExtraInfo()
                        }
                    }
                }
            };
            var gch = GCHandle.Alloc(inputs, GCHandleType.Pinned);
            SendInput((uint)inputs.Length, gch.AddrOfPinnedObject(), Marshal.SizeOf(typeof(Input)));
            gch.Free();
        }
        #endregion
    }
}
