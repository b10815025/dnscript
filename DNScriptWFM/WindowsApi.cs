﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing;

namespace DNScriptWFM
{

    public static partial class WindowsApi
    {
        
        #region Win32API_Method
        [DllImport("user32.dll")]
        public static extern bool SetCursorPos(int X, int Y);
        [DllImport("user32.dll")]
        public static extern void mouse_event(MouseEventFlag flag, int dx, int dy, int data, UIntPtr extraInfo);
        [DllImport("user32.dll", SetLastError = true)]
        private static extern uint SendInput(uint nInput, IntPtr pIntputs, int cbSize);
        [DllImport("user32.dll")]
        private static extern IntPtr GetMessageExtraInfo();
        [DllImport("user32.dll", EntryPoint = "FindWindow", SetLastError = true)]
        public static extern IntPtr FindWindow(string lpclass, string lpwindowName);
        [DllImport("user32.dll")]
        public static extern bool GetWindowRect(IntPtr hwnd, out NativeRECT rect);
        #endregion

        
    }
}
