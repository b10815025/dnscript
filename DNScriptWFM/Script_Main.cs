﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
namespace DNScriptWFM
{
    public partial class Form1 : Form
    {
        public bool Stop_Interrupt(object sender, DoWorkEventArgs e)
        {
            if (bw.CancellationPending)
            {
                for (int i = 0; i < 100; i++)
                {
                    e.Cancel = true;
                    LoopTextReadFlag = false;
                    return true;
                }
            }
            return false;
        }
        public void Start(object sender, DoWorkEventArgs e)
        {
            #region Write
            var outPutDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);
            String path = outPutDirectory + "\\config.txt";
            String path_2_5 = outPutDirectory + "\\config2.5.txt";
            path = path.Remove(0, 6);
            path_2_5 = path_2_5.Remove(0, 6);
            int bingo_color32 = bingo.ToArgb();
            int mailbox_color32 = mailbox.ToArgb();
            int Character_color32 = CharacterReturn.ToArgb();
            String[] SquareNum = { NumLeftTop.Text, NumTop.Text, NumRightTop.Text, NumLeft.Text, NumCenter.Text, NumRight.Text, NumLeftBottom.Text, NumBottom.Text, NumRightBottom.Text }; ;

            String w = LoginX.Text + ' ' + LoginY.Text + '\n' + Character1PosX.Text + ' ' + Character1PosY.Text + '\n' + RadioPosX.Text + ' '
                + RadioPosY.Text + '\n' + OnlinePosX.Text + ' ' + OnlinePosY.Text + '\n' + BingoPosX.Text + ' ' + BingoPosY.Text + '\n'
                + BingoInputPosX.Text + ' ' + BingoInputPosY.Text + '\n' + AssignInputPosX.Text + ' ' + AssignInputPosY.Text + '\n'
                + ReturnCharacterPosX.Text + ' ' + ReturnCharacterPosY.Text + '\n' + EventButtonPosX.Text + ' ' + EventButtonPosY.Text + '\n'
                + MailPosX.Text + ' ' + MailPosY.Text + '\n' + bingo_color32.ToString() + '\n' + mailbox_color32.ToString() + '\n'
                + Character_color32.ToString() + '\n';
            String w_2_5 = NineSquarePosX.Text + ' ' + NineSquarePosY.Text + '\n' + "X : " + resolutionXBox.Text + ' '
                + " Y : " + resolutionYBox.Text + '\n';
            File.WriteAllText(path, w);
            File.WriteAllText(path_2_5, w_2_5);
            Interval(1);
            #endregion
            #region ReadPosition
            //start after 1s   
            Point Character1 = new Point();
            Point Login = new Point();
            Point Radio = new Point();
            Point Online = new Point();
            Point Bingo = new Point();
            Point BingoInput = new Point();
            Point AssignInput = new Point();
            Point ReturnCharacter = new Point();
            Point EventPage = new Point();
            Point MailBox = new Point();
            Point Resolution = new Point();
            Point nowPos = new Point();
            CharacterTotal = Int32.Parse(CharacterTotalBox.Text);
            String s1x = Character1PosX.Text.Remove(0, 4);
            String s1y = Character1PosY.Text.Remove(0, 4);
            String L1x = LoginX.Text.Remove(0, 4);
            String L1y = LoginY.Text.Remove(0, 4);
            String r1x = RadioPosX.Text.Remove(0, 4);
            String r1y = RadioPosY.Text.Remove(0, 4);
            String o1x = OnlinePosX.Text.Remove(0, 4);
            String o1y = OnlinePosY.Text.Remove(0, 4);
            String b1x = BingoPosX.Text.Remove(0, 4);
            String b1y = BingoPosY.Text.Remove(0, 4);
            String b2x = BingoInputPosX.Text.Remove(0, 4);
            String b2y = BingoInputPosY.Text.Remove(0, 4);
            String a1x = AssignInputPosX.Text.Remove(0, 4);
            String a1y = AssignInputPosY.Text.Remove(0, 4);
            String r2x = ReturnCharacterPosX.Text.Remove(0, 4);
            String r2y = ReturnCharacterPosY.Text.Remove(0, 4);
            String e1x = EventButtonPosX.Text.Remove(0, 4);
            String e1y = EventButtonPosY.Text.Remove(0, 4);
            String m1x = MailPosX.Text.Remove(0, 4);
            String m1y = MailPosY.Text.Remove(0, 4);
            String p1x = NineSquarePosX.Text.Remove(0, 4);
            String p1y = NineSquarePosY.Text.Remove(0, 4);
            int offset = Int32.Parse(Offset.Text);
            int IntervalOfAssign = Int32.Parse(IntervalOfNumAssign.Text);
            Character1.X = Int32.Parse(s1x);
            Character1.Y = Int32.Parse(s1y);
            Login.X = Int32.Parse(L1x);
            Login.Y = Int32.Parse(L1y);
            Radio.X = Int32.Parse(r1x);
            Radio.Y = Int32.Parse(r1y);
            Online.X = Int32.Parse(o1x);
            Online.Y = Int32.Parse(o1y);
            Bingo.X = Int32.Parse(b1x);
            Bingo.Y = Int32.Parse(b1y);
            BingoInput.X = Int32.Parse(b2x);
            BingoInput.Y = Int32.Parse(b2y);
            AssignInput.X = Int32.Parse(a1x);
            AssignInput.Y = Int32.Parse(a1y);
            ReturnCharacter.X = Int32.Parse(r2x);
            ReturnCharacter.Y = Int32.Parse(r2y);
            EventPage.X = Int32.Parse(e1x);
            EventPage.Y = Int32.Parse(e1y);
            MailBox.X = Int32.Parse(m1x);
            MailBox.Y = Int32.Parse(m1y);
            CharacterNum = offset;
            #endregion
            #region NineSquarePosition
            HashSet<int> setOfNum = new HashSet<int>();
            List<int> listNum = new List<int>();
            if (AutoNumChecked.Checked == true)
            {
                Resolution.X = Int32.Parse(resolutionXBox.Text);
                Resolution.Y = Int32.Parse(resolutionYBox.Text);

                for (int i = 0; i < 9; i++)
                {
                    SquareDirection[i].X = Int32.Parse(p1x);
                    SquareDirection[i].Y = Int32.Parse(p1y);
                }
                NineSquareDivision.Square(SquareDirection, Resolution);
            }
            #endregion
            #region MakeEnumType
            String OthersKey = "DIK_" + IndexOfOthers.Text;
            String EventPageKey = "DIK_" + IndexOfEventPage.Text;
            WindowsApi.DirectXKeyStrokes otherKey = (WindowsApi.DirectXKeyStrokes)Enum.Parse(typeof(WindowsApi.DirectXKeyStrokes), OthersKey, true);
            WindowsApi.DirectXKeyStrokes eventPageKey = (WindowsApi.DirectXKeyStrokes)Enum.Parse(typeof(WindowsApi.DirectXKeyStrokes), EventPageKey, true);
            #endregion
            #region Loop_ReturnAfterfirst
            if (LoopCheckBox.Checked == true && LoopIndex > 1)
            {
                if (EscapeBox.Checked == true)
                {
                    //ESC
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_ESCAPE, false);
                    Interval(1);
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_ESCAPE, true);
                    Interval(1);
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_ESCAPE, false);
                    Interval(1);
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_ESCAPE, true);
                    Interval(1);
                }

                if (Stop_Interrupt(sender, e)) return;
                WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_ESCAPE, false);
                Interval(1);
                WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_ESCAPE, true);
                Interval(1);
                if (Stop_Interrupt(sender, e)) return;
                WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_ESCAPE, false);
                Interval(1);
                WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_ESCAPE, true);
                Interval(1);


                if (Stop_Interrupt(sender, e)) return;
                //ReturnCharacter
                nowPos = ReturnCharacter;
                WindowsApi.SetCursorPos(nowPos.X, nowPos.Y);
                WindowsApi.mouse_event(WindowsApi.MouseEventFlag.LeftDown | WindowsApi.MouseEventFlag.LeftUp, nowPos.X, nowPos.Y, 0, UIntPtr.Zero);
                Interval(2);
                if (Stop_Interrupt(sender, e)) return;
                WindowsApi.SetCursorPos(MousePosition.X, MousePosition.Y);
                WindowsApi.mouse_event(WindowsApi.MouseEventFlag.LeftDown | WindowsApi.MouseEventFlag.LeftUp, MousePosition.X, MousePosition.Y, 0, UIntPtr.Zero);
                Interval(2);
                if (ScanReturn(Character1, e))
                {
                    Interval(2);
                }

            }
            #endregion
            #region MainScript
            for (int i = 0; i < CharacterTotal - offset + 1; i++, CharacterNum++)
            {

                #region CheckStatus
                if (bw.CancellationPending)
                {
                    e.Cancel = true;
                    if (IsKeyLocked(Keys.NumLock) == false)
                    {
                        //If NumLock is On then turn Off
                        WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_NUMLOCK, false);
                        Interval(1);
                        WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_NUMLOCK, true);
                    }
                    return;
                }
                if (IsKeyLocked(Keys.NumLock) == true)
                {
                    //If NumLock is On then turn Off
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_NUMLOCK, false);
                    Interval(1);
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_NUMLOCK, true);
                }
                #endregion

                //list:character->login->radio->online->bingo->input->random->confirm->Esc->return
                //character
                if (Stop_Interrupt(sender, e)) return;
                nowPos = Character1;
                WindowsApi.SetCursorPos(nowPos.X, nowPos.Y);
                WindowsApi.mouse_event(WindowsApi.MouseEventFlag.LeftDown | WindowsApi.MouseEventFlag.LeftUp, nowPos.X, nowPos.Y, 0, UIntPtr.Zero);


                Interval(1);
                #region SwitchCharacterOfIndex
                if (Stop_Interrupt(sender, e)) return;                
                for (int j = 0; j < i + offset - 1; j++)
                {
                    if (Stop_Interrupt(sender, e)) return;
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_DOWN, false);
                    if (Interval(2)) ;
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_DOWN, true);
                }
                if (BugMode.Checked == true)
                {
                    if (Stop_Interrupt(sender, e)) return;
                    nowPos = Login;
                    WindowsApi.SetCursorPos(nowPos.X, nowPos.Y);
                    WindowsApi.mouse_event(WindowsApi.MouseEventFlag.LeftDown | WindowsApi.MouseEventFlag.LeftUp, nowPos.X, nowPos.Y, 0, UIntPtr.Zero);
                    Interval(4);
                }
                #endregion
                Interval(2);
                if (Stop_Interrupt(sender, e)) return;
                //login
                nowPos = Login;
                WindowsApi.SetCursorPos(nowPos.X, nowPos.Y);
                WindowsApi.mouse_event(WindowsApi.MouseEventFlag.LeftDown | WindowsApi.MouseEventFlag.LeftUp, nowPos.X, nowPos.Y, 0, UIntPtr.Zero);
                Interval(1);
                //Thread.Sleep(500);
                if (Stop_Interrupt(sender, e)) return;
                //radio
                nowPos = Radio;
                WindowsApi.SetCursorPos(nowPos.X, nowPos.Y);
                WindowsApi.mouse_event(WindowsApi.MouseEventFlag.LeftDown | WindowsApi.MouseEventFlag.LeftUp, nowPos.X, nowPos.Y, 0, UIntPtr.Zero);
                Interval(1);
                if (Stop_Interrupt(sender, e)) return;
                //online
                nowPos = Online;
                WindowsApi.SetCursorPos(nowPos.X, nowPos.Y);
                WindowsApi.mouse_event(WindowsApi.MouseEventFlag.LeftDown | WindowsApi.MouseEventFlag.LeftUp, nowPos.X, nowPos.Y, 0, UIntPtr.Zero);
                Interval(1);
                if (Stop_Interrupt(sender, e)) return;
                if (!getGiftFlag)
                {
                    nowPos = MailBox;
                    WindowsApi.SetCursorPos(nowPos.X, nowPos.Y);
                    if (ScanPass(MailBox, e))
                    {
                        Interval(1);
                    }

                    Stop_Interrupt(sender, e);
                    //cancelCtrl if task trace is off
                    if (taskTraceFlag)
                    {
                        if (Stop_Interrupt(sender, e)) return;
                        WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_LCONTROL, false);
                        Interval(1);
                        WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_LCONTROL, true);
                        Interval(1);
                    }
                    if (Stop_Interrupt(sender, e)) return;
                    //bingo
                    nowPos = Bingo;
                    WindowsApi.SetCursorPos(nowPos.X, nowPos.Y);
                    WindowsApi.mouse_event(WindowsApi.MouseEventFlag.LeftDown | WindowsApi.MouseEventFlag.LeftUp, nowPos.X, nowPos.Y, 0, UIntPtr.Zero);
                    Interval(1);
                    if (Stop_Interrupt(sender, e)) return;
                    WindowsApi.SetCursorPos(nowPos.X, nowPos.Y);
                    WindowsApi.mouse_event(WindowsApi.MouseEventFlag.LeftDown | WindowsApi.MouseEventFlag.LeftUp, nowPos.X, nowPos.Y, 0, UIntPtr.Zero);
                    Interval(1);
                    //interrupt start

                    if (!interruptFlag)
                    {
                        if (Stop_Interrupt(sender, e)) return;
                        //input-1
                        nowPos = AssignInput;
                        WindowsApi.SetCursorPos(nowPos.X, nowPos.Y);
                        WindowsApi.mouse_event(WindowsApi.MouseEventFlag.LeftDown | WindowsApi.MouseEventFlag.LeftUp, nowPos.X, nowPos.Y, 0, UIntPtr.Zero);
                        Interval(1);
                        if (Stop_Interrupt(sender, e)) return;
                        if (AutoNumChecked.Checked == true && LoopIndex == 1)
                        {                            
                            //auto assign
                            foreach (String s in SquareNum)
                            {
                                int checkAdd = setOfNum.Count;
                                if (s != "")
                                {
                                    setOfNum.Add(Int32.Parse(s));
                                    if (setOfNum.Count == checkAdd)//if duplicate
                                    {
                                        MessageBox.Show("有重複數字");
                                        bw.CancelAsync();
                                        Interval(1);
                                        e.Cancel = true;
                                        setOfNum.Clear();
                                        listNum.Clear();
                                        return;
                                    }
                                    listNum.Add(Int32.Parse(s));
                                }
                                else
                                {
                                    MessageBox.Show("有空格ˋˊ");
                                    bw.CancelAsync();
                                    Interval(1);
                                    e.Cancel = true;
                                    setOfNum.Clear();
                                    listNum.Clear();
                                    return;
                                }
                            }
                            if (Stop_Interrupt(sender, e)) return;
                            for (int index = 0; index < 9; index++)
                            {
                                if (Stop_Interrupt(sender, e)) return;
                                NineSquareDivision.setNumber(SquareDirection, listNum, index, IntervalOfAssign);
                            }
                            setOfNum.Clear();
                            listNum.Clear();
                            Interval(1);
                            //ESC                            
                        }
                        else if (LoopIndex == 1 && AutoNumChecked.Checked == false)
                        {
                            //random
                            nowPos = BingoInput;
                            WindowsApi.SetCursorPos(nowPos.X, nowPos.Y);
                            WindowsApi.mouse_event(WindowsApi.MouseEventFlag.LeftDown | WindowsApi.MouseEventFlag.LeftUp, nowPos.X, nowPos.Y, 0, UIntPtr.Zero);
                            Interval(1);
                        }
                        if (Stop_Interrupt(sender, e)) return;
                        //input-2
                        nowPos = AssignInput;
                        WindowsApi.SetCursorPos(nowPos.X, nowPos.Y);
                        WindowsApi.mouse_event(WindowsApi.MouseEventFlag.LeftDown | WindowsApi.MouseEventFlag.LeftUp, nowPos.X, nowPos.Y, 0, UIntPtr.Zero);
                        Interval(2);
                        if (AutoNumChecked.Checked == true)
                        {
                            //ESC
                            WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_ESCAPE, false);
                            Interval(1);
                            WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_ESCAPE, true);
                            Interval(1);
                            //LCTRL
                            if (Stop_Interrupt(sender, e)) return;
                            WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_LCONTROL, false);
                            Interval(1);
                            WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_LCONTROL, true);
                            Interval(1);
                        }
                    }
                    if (Stop_Interrupt(sender, e)) return;
                    nowPos = Bingo;
                    WindowsApi.SetCursorPos(nowPos.X, nowPos.Y);
                    WindowsApi.mouse_event(WindowsApi.MouseEventFlag.LeftDown | WindowsApi.MouseEventFlag.LeftUp, nowPos.X, nowPos.Y, 0, UIntPtr.Zero);
                    Interval(1);
                    //bingo
                    if (Stop_Interrupt(sender, e)) return;
                    nowPos = Bingo;
                    WindowsApi.SetCursorPos(nowPos.X, nowPos.Y);
                    WindowsApi.mouse_event(WindowsApi.MouseEventFlag.LeftDown | WindowsApi.MouseEventFlag.LeftUp, nowPos.X, nowPos.Y, 0, UIntPtr.Zero);
                    Interval(2);
                    if (Stop_Interrupt(sender, e)) return;
                    nowPos = Bingo;
                    WindowsApi.SetCursorPos(nowPos.X, nowPos.Y);
                    WindowsApi.mouse_event(WindowsApi.MouseEventFlag.LeftDown | WindowsApi.MouseEventFlag.LeftUp, nowPos.X, nowPos.Y, 0, UIntPtr.Zero);
                    Interval(2);

                }
                else
                {
                    if (((IndexOfOthers.Text[0] >= '0' && IndexOfOthers.Text[0] <= '9') || IndexOfEventPage.Text[0] >= '0' && IndexOfEventPage.Text[0] <= '9') && IsKeyLocked(Keys.NumLock) == false)
                    {
                        //Turn on numlock
                        if (Stop_Interrupt(sender, e)) return;
                        WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_NUMLOCK, false);
                        Interval(1);
                        WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_NUMLOCK, true);

                    }
                    if (Stop_Interrupt(sender, e)) return;
                    nowPos = MailBox;
                    WindowsApi.SetCursorPos(nowPos.X, nowPos.Y);
                    if (ScanPass(MailBox, e)) ;
                    if (!taskTraceFlag)
                    {
                        if (Stop_Interrupt(sender, e)) return;
                        //interrupt Start
                        WindowsApi.SendKey(otherKey, false);
                        Interval(2);
                        WindowsApi.SendKey(otherKey, true);
                        Interval(2);
                    }
                    if (Stop_Interrupt(sender, e)) return;
                    WindowsApi.SendKey(otherKey, false);
                    Interval(2);
                    WindowsApi.SendKey(otherKey, true);
                    Interval(2);
                    if (Stop_Interrupt(sender, e)) return;
                    WindowsApi.SendKey(eventPageKey, false);
                    Interval(2);
                    WindowsApi.SendKey(eventPageKey, true);
                    Interval(2);
                    if (Stop_Interrupt(sender, e)) return;
                    nowPos = EventPage;
                    WindowsApi.SetCursorPos(nowPos.X, nowPos.Y);
                    WindowsApi.mouse_event(WindowsApi.MouseEventFlag.LeftDown | WindowsApi.MouseEventFlag.LeftUp, nowPos.X, nowPos.Y, 0, UIntPtr.Zero);
                    Interval(1);
                    if (Stop_Interrupt(sender, e)) return;
                    nowPos = AssignInput;
                    WindowsApi.SetCursorPos(nowPos.X, nowPos.Y);
                    WindowsApi.mouse_event(WindowsApi.MouseEventFlag.LeftDown | WindowsApi.MouseEventFlag.LeftUp, nowPos.X, nowPos.Y, 0, UIntPtr.Zero);
                    Interval(2);

                }
                //interrupt end

                if (EscapeBox.Checked == true && AutoNumChecked.Checked == false)
                {
                    // Interval(2);
                    //ESC
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_ESCAPE, false);
                    Interval(2);
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_ESCAPE, true);
                    Interval(2);
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_ESCAPE, false);
                    Interval(2);
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_ESCAPE, true);
                    Interval(2);

                }


                if (Stop_Interrupt(sender, e)) e.Cancel = true;
                if (AutoNumChecked.Checked == false)
                {
                    //ESC
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_ESCAPE, false);
                    Interval(2);
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_ESCAPE, true);
                    Interval(2);
                }

                if (Stop_Interrupt(sender, e)) return;
                WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_ESCAPE, false);
                Interval(2);
                WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_ESCAPE, true);
                Interval(2);


                if (Stop_Interrupt(sender, e)) return;
                //ReturnCharacter
                nowPos = ReturnCharacter;
                WindowsApi.SetCursorPos(nowPos.X, nowPos.Y);
                WindowsApi.mouse_event(WindowsApi.MouseEventFlag.LeftDown | WindowsApi.MouseEventFlag.LeftUp, nowPos.X, nowPos.Y, 0, UIntPtr.Zero);
                Interval(2);
                if (Stop_Interrupt(sender, e)) return;
                WindowsApi.SetCursorPos(MousePosition.X, MousePosition.Y);
                WindowsApi.mouse_event(WindowsApi.MouseEventFlag.LeftDown | WindowsApi.MouseEventFlag.LeftUp, MousePosition.X, MousePosition.Y, 0, UIntPtr.Zero);
                Interval(2);
                if (ScanReturn(Character1, e))
                {
                    Interval(2);
                }
            }
            #endregion
            #region Undisconnected For User And Loop
            if (UnDisconnectBox.Checked == true || LoopCheckBox.Checked == true)
            {
                if (BugMode.Checked == true)
                {
                    if (Stop_Interrupt(sender, e)) return;
                    nowPos = Login;
                    WindowsApi.SetCursorPos(nowPos.X, nowPos.Y);
                    WindowsApi.mouse_event(WindowsApi.MouseEventFlag.LeftDown | WindowsApi.MouseEventFlag.LeftUp, nowPos.X, nowPos.Y, 0, UIntPtr.Zero);
                    Interval(4);
                }
                Interval(2);
                //login
                nowPos = Login;
                WindowsApi.SetCursorPos(nowPos.X, nowPos.Y);
                WindowsApi.mouse_event(WindowsApi.MouseEventFlag.LeftDown | WindowsApi.MouseEventFlag.LeftUp, nowPos.X, nowPos.Y, 0, UIntPtr.Zero);
                Interval(1);
                //Thread.Sleep(500);
                //radio
                nowPos = Radio;
                WindowsApi.SetCursorPos(nowPos.X, nowPos.Y);
                WindowsApi.mouse_event(WindowsApi.MouseEventFlag.LeftDown | WindowsApi.MouseEventFlag.LeftUp, nowPos.X, nowPos.Y, 0, UIntPtr.Zero);
                Interval(1);
                //online
                nowPos = Online;
                WindowsApi.SetCursorPos(nowPos.X, nowPos.Y);
                WindowsApi.mouse_event(WindowsApi.MouseEventFlag.LeftDown | WindowsApi.MouseEventFlag.LeftUp, nowPos.X, nowPos.Y, 0, UIntPtr.Zero);
                Interval(1);
            }
            #endregion
            #region Exit
            if (bw.IsBusy)
            {
                bw.CancelAsync();
            }
            for (int i = 0; i < 100; i++)
            {
                if (bw.CancellationPending)
                {
                    e.Cancel = true; //thread kill 
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_NUMLOCK, false);
                    Interval(1);
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_NUMLOCK, true);
                    CharacterNum = offset;
                    if (LoopIndex == LoopCount && LoopCheckBox.Checked == true)
                    {
                        LoopTimer.Enabled = false;
                        TimeTotal = 0;
                        MessageBox.Show("好了啦 下次一定");
                    }
                    else
                    {
                        // MessageBox.Show("好了啦 下次肯定");
                    }
                    LoopIndex++;
                    return;
                }
            }
            #endregion
        }
    }
}
