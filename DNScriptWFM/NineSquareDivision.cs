﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime;

namespace DNScriptWFM
{


    public class NineSquareDivision
    {
        public enum Direction
        {
            LeftTop = 0,
            Top = 1,
            RightTop = 2,
            Left = 3,
            Center = 4,
            Right = 5,
            BottomLeft = 6,
            Bottom = 7,
            BottomRight = 8
        }
        public static bool Interval(int n, int ms)
        {            
            int totalDelay = ms * n;
            Thread.Sleep(totalDelay);
            return true;
        }
        public static void Square(Point[] SquarePoint, Point Resolution)
        {
            Point defaut_Delta = new Point(107, 107);
            if ((Resolution.X == 0 && Resolution.Y == 0) || (SquarePoint[(int)Direction.LeftTop].X == 0 && SquarePoint[(int)Direction.LeftTop].Y == 0))
            {
                MessageBox.Show("請輸入解析度或賓果左上座標");
                return;
            }
            double deltaX = defaut_Delta.X * ((double)Resolution.X / 1920.0);
            double deltaY = defaut_Delta.Y * ((double)Resolution.Y / 1080.0);
            
            int dx = (int)Math.Round(deltaX);
            int dy = (int)Math.Round(deltaY);
            //Top
            SquarePoint[(int)Direction.Top].X += dx;
            //RightTop
            SquarePoint[(int)Direction.RightTop].X += 2 * dx;
            //Left
            SquarePoint[(int)Direction.Left].Y += dy;
            //Center
            SquarePoint[(int)Direction.Center].X += dx;
            SquarePoint[(int)Direction.Center].Y += dy;
            //Right
            SquarePoint[(int)Direction.Right].X += 2 * dx;
            SquarePoint[(int)Direction.Right].Y += dy;
            //BottomLeft
            SquarePoint[(int)Direction.BottomLeft].Y += 2 * dy;
            //Bottom
            SquarePoint[(int)Direction.Bottom].X += dx;
            SquarePoint[(int)Direction.Bottom].Y += 2 * dy;
            //BottomRight
            SquarePoint[(int)Direction.BottomRight].X += 2 * dx;
            SquarePoint[(int)Direction.BottomRight].Y += 2 * dy;
        }
        public static void setNumber(Point[] SquarePoints, List<int> listNum, int i, int IntervalOfNum)
        {
            Point nowPos = new Point();

            nowPos = SquarePoints[i];
            int number = listNum[i];
            Interval(1, IntervalOfNum);
            WindowsApi.SetCursorPos(nowPos.X, nowPos.Y);
            WindowsApi.mouse_event(WindowsApi.MouseEventFlag.LeftDown | WindowsApi.MouseEventFlag.LeftUp, nowPos.X, nowPos.Y, 0, UIntPtr.Zero);
            Interval(1, IntervalOfNum);
            switch (number)
            {
                case 0:
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_0, false);
                    Interval(1, IntervalOfNum);
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_0, true);
                    Interval(1, IntervalOfNum);
                    break;
                case 1:
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_1, false);
                    Interval(1, IntervalOfNum);
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_1, true);
                    Interval(1, IntervalOfNum);
                    break;
                case 2:
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_2, false);
                    Interval(1, IntervalOfNum);
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_2, true);
                    Interval(1, IntervalOfNum);
                    break;
                case 3:
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_3, false);
                    Interval(1, IntervalOfNum);
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_3, true);
                    Interval(1, IntervalOfNum);
                    break;
                case 4:
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_4, false);
                    Interval(1, IntervalOfNum);
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_4, true);
                    Interval(1, IntervalOfNum);
                    break;
                case 5:
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_5, false);
                    Interval(1, IntervalOfNum);
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_5, true);
                    Interval(1, IntervalOfNum);
                    break;
                case 6:
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_6, false);
                    Interval(1, IntervalOfNum);
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_6, true);
                    Interval(1, IntervalOfNum);
                    break;
                case 7:
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_7, false);
                    Interval(1, IntervalOfNum);
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_7, true);
                    Interval(1, IntervalOfNum);
                    break;
                case 8:
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_8, false);
                    Interval(1, IntervalOfNum);
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_8, true);
                    Interval(1, IntervalOfNum);
                    break;
                case 9:
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_9, false);
                    Interval(1, IntervalOfNum);
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_9, true);
                    Interval(1, IntervalOfNum);
                    break;
                case 10:
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_1, false);
                    Interval(1, IntervalOfNum);
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_1, true);
                    Interval(1, IntervalOfNum);
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_0, false);
                    Interval(1, IntervalOfNum);
                    WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_0, true);
                    Interval(1, IntervalOfNum);
                    break;
                default: break;
            }

        }
    }
}
