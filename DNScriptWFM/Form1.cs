﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;


namespace DNScriptWFM
{
    public partial class Form1 : Form
    {     

        public Form1()
        {
            InitializeComponent();
        }

        #region ObjectEvent
        private void LoginConfirm_Click(object sender, EventArgs e)
        {
            getLoginPos = true;

        }
        private void Character1_Click(object sender, EventArgs e)
        {
            getCharacter1Pos = true;
        }
        private void Radio_Click(object sender, EventArgs e)
        {
            getRadioPos = true;
        }

        private void Online_Click(object sender, EventArgs e)
        {
            getOnlinePos = true;
        }

        private void Bingo_Click(object sender, EventArgs e)
        {
            getBingoPos = true;
        }

        private void BingoInput_Click(object sender, EventArgs e)
        {
            getBingoInputPos = true;
        }

        private void AssignBingo_Click(object sender, EventArgs e)
        {
            getAssignInputPos = true;
        }
        private void ReturnCharacter_Click(object sender, EventArgs e)
        {
            getReturnCharacterPos = true;
        }
        private void EventButton_Click(object sender, EventArgs e)
        {
            getEventPos = true;
        }
        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar == 'A' || e.KeyChar == 'a'))
            {
                if (getLoginPos)
                {
                    LoginX.Text = "X : " + MousePosition.X;
                    LoginY.Text = "Y : " + MousePosition.Y;
                    getLoginPos = false;
                }
                if (getCharacter1Pos)
                {
                    Character1PosX.Text = "X : " + MousePosition.X;
                    Character1PosY.Text = "Y : " + MousePosition.Y;
                    getCharacter1Pos = false;
                    CharacterReturn = getPixel(MousePosition);
                }
                if (getRadioPos)
                {
                    RadioPosX.Text = "X : " + MousePosition.X;
                    RadioPosY.Text = "Y : " + MousePosition.Y;
                    getRadioPos = false;
                }
                if (getOnlinePos)
                {
                    OnlinePosX.Text = "X : " + MousePosition.X;
                    OnlinePosY.Text = "Y : " + MousePosition.Y;
                    getOnlinePos = false;
                }
                if (getBingoPos)
                {
                    BingoPosX.Text = "X : " + MousePosition.X;
                    BingoPosY.Text = "Y : " + MousePosition.Y;
                    getBingoPos = false;
                    bingo = getPixel(MousePosition);
                }
                if (getBingoInputPos)
                {
                    BingoInputPosX.Text = "X : " + MousePosition.X;
                    BingoInputPosY.Text = "Y : " + MousePosition.Y;
                    getBingoInputPos = false;
                }
                if (getAssignInputPos)
                {
                    AssignInputPosX.Text = "X : " + MousePosition.X;
                    AssignInputPosY.Text = "Y : " + MousePosition.Y;
                    getAssignInputPos = false;
                }
                if (getReturnCharacterPos)
                {
                    ReturnCharacterPosX.Text = "X : " + MousePosition.X;
                    ReturnCharacterPosY.Text = "Y : " + MousePosition.Y;
                    getReturnCharacterPos = false;
                }
                if (getEventPos)
                {
                    EventButtonPosX.Text = "X : " + MousePosition.X;
                    EventButtonPosY.Text = "Y : " + MousePosition.Y;
                    getEventPos = false;
                }
                if (getMailPos)
                {
                    MailPosX.Text = "X : " + MousePosition.X;
                    MailPosY.Text = "Y : " + MousePosition.Y;
                    getMailPos = false;
                    mailbox = getPixel(MousePosition);
                }
                if (getNineSquareLeftTop)
                {
                    NineSquarePosX.Text = "X : " + MousePosition.X;
                    NineSquarePosY.Text = "Y : " + MousePosition.Y;
                    getNineSquareLeftTop = false;
                }
            }
            if (e.KeyChar == 'S' || e.KeyChar == 's')
            {
                if (bw.IsBusy)
                {
                    bw.CancelAsync();
                }
                if (LoopTimer.Enabled == true)
                {
                    TimeTotal = 0;
                    LoopIndex = 1;
                    LoopTimer.Enabled = false;
                }
            }
        }
        private void StartButton_Click(object sender, EventArgs e)
        {
            if (LoopCheckBox.Checked == true)
            {
                LoopEnabled = true;
                LoopTimer.Enabled = true;
            }
            else
            {
                LoopEnabled = false;
                LoopTimer.Enabled = false;
            }
            if (!LoopTextReadFlag && LoopIndex == 1)
            {
                LoopCount = Int32.Parse(LoopCountBox.Text);
                LoopTextReadFlag = true;
            }

            bw.DoWork += new DoWorkEventHandler(Start);
            if (!bw.IsBusy)
            {
                bw.RunWorkerAsync();
            }
        }
        private void getBonus_CheckedChanged(object sender, EventArgs e)
        {
            if (getBonus.Checked == true)
            {
                getGiftFlag = true;
            }
            else
            {
                getGiftFlag = false;
            }

        }
        private void MailButton_Click(object sender, EventArgs e)
        {
            getMailPos = true;
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            var outPutDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);
            String path = outPutDirectory + "\\config.txt";
            String path_2_5 = outPutDirectory + "\\config2.5.txt";
            path = path.Remove(0, 6);
            path_2_5 = path_2_5.Remove(0, 6);
            CharacterNum = 1;
            if (File.Exists(path))
            {
                String s = File.ReadAllText(path);
                for (int i = 0, index = 0; i < s.Length; i++)
                {

                    if (i + 1 < s.Length)
                    {
                        if (s[0] == '\r' || s[0] == '\0' || s[0] == '\n') s.Remove(0, 1);
                        if (s[i + 1] == 'Y')
                        {
                            String spilt = s.Substring(0, i);
                            s = s.Remove(0, i + 1);
                            switch (index)
                            {
                                case 0:
                                    LoginX.Text = spilt;
                                    i = 0; break;
                                case 1:
                                    Character1PosX.Text = spilt;
                                    i = 0; break;
                                case 2:
                                    RadioPosX.Text = spilt;
                                    i = 0; break;
                                case 3:
                                    OnlinePosX.Text = spilt;
                                    i = 0; break;
                                case 4:
                                    BingoPosX.Text = spilt;
                                    i = 0; break;
                                case 5:
                                    BingoInputPosX.Text = spilt;
                                    i = 0; break;
                                case 6:
                                    AssignInputPosX.Text = spilt;
                                    i = 0; break;
                                case 7:
                                    ReturnCharacterPosX.Text = spilt;
                                    i = 0; break;
                                case 8:
                                    EventButtonPosX.Text = spilt;
                                    i = 0; break;
                                case 9:
                                    MailPosX.Text = spilt;
                                    i = 0; break;
                            }
                        }
                        if ((s[i + 1] == '\n' || s[i + 1] == '\0') && index < 10)

                        {
                            String spilt = s.Substring(0, i + 1);
                            s = s.Remove(0, i + 2);
                            switch (index)
                            {
                                case 0:
                                    LoginY.Text = spilt;
                                    i = 0; break;
                                case 1:
                                    Character1PosY.Text = spilt;
                                    i = 0; break;
                                case 2:
                                    RadioPosY.Text = spilt;
                                    i = 0; break;
                                case 3:
                                    OnlinePosY.Text = spilt;
                                    i = 0; break;
                                case 4:
                                    BingoPosY.Text = spilt;
                                    i = 0; break;
                                case 5:
                                    BingoInputPosY.Text = spilt;
                                    i = 0; break;
                                case 6:
                                    AssignInputPosY.Text = spilt;
                                    i = 0; break;
                                case 7:
                                    ReturnCharacterPosY.Text = spilt;
                                    i = 0; break;
                                case 8:
                                    EventButtonPosY.Text = spilt;
                                    i = 0; break;
                                case 9:
                                    MailPosY.Text = spilt;
                                    i = 0; break;
                                default: break;
                            }
                            if (index < 10) index++;
                        }
                        if ((s[i + 1] == ' ' || s[i + 1] == '\n' || s[i + 1] == '\0') && index >= 10)
                        {
                            String spilt = s.Substring(0, i + 1);
                            s = s.Remove(0, i + 1);
                            if (index == 10)
                            {
                                int color = Int32.Parse(spilt);
                                bingo = Color.FromArgb(color);
                                i = 0;
                                index++;
                            }
                            else if (index == 11)
                            {
                                int color = Int32.Parse(spilt);
                                mailbox = Color.FromArgb(color);
                                i = 0;
                                index++;
                            }
                            else if (index == 12)
                            {
                                int color = Int32.Parse(spilt);
                                CharacterReturn = Color.FromArgb(color);
                                i = 0;
                            }

                        }
                    }

                }
            }

            if (File.Exists(path_2_5))
            {
                String s_2_5 = File.ReadAllText(path_2_5);
                for (int i = 0, index = 0; i < s_2_5.Length; i++)
                {
                    if (i + 1 < s_2_5.Length)
                    {
                        if (s_2_5[0] == '\r' || s_2_5[0] == '\0' || s_2_5[0] == '\n') s_2_5.Remove(0, 1);
                        if (s_2_5[i + 1] == 'Y')
                        {
                            String spilt = s_2_5.Substring(0, i);
                            s_2_5 = s_2_5.Remove(0, i + 1);
                            switch (index)
                            {
                                case 0:
                                    NineSquarePosX.Text = spilt;
                                    i = 0; break;
                                case 1:
                                    spilt = spilt.Remove(0, 4);
                                    resolutionXBox.Text = spilt;
                                    i = 0; break;
                                default:
                                    break;
                            }
                        }
                        if ((s_2_5[i + 1] == '\n' || s_2_5[i + 1] == '\0'))
                        {
                            String spilt = s_2_5.Substring(0, i + 1);
                            s_2_5 = s_2_5.Remove(0, i + 2);
                            switch (index)
                            {
                                case 0:
                                    NineSquarePosY.Text = spilt;
                                    index++;
                                    i = 0; break;
                                case 1:
                                    spilt = spilt.Remove(0, 4);
                                    resolutionYBox.Text = spilt;
                                    i = 0; break;
                                default:
                                    break;
                            }
                        }
                    }

                }
            }


            bw.WorkerSupportsCancellation = true;

        }
        private void Form1_FormClosing(object sender, EventArgs e)
        {
            if (IsKeyLocked(Keys.NumLock) == false)
            {
                WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_NUMLOCK, false);
                Interval(1);
                WindowsApi.SendKey(WindowsApi.DirectXKeyStrokes.DIK_NUMLOCK, true);
            }
        }
        private void Stop_Click(object sender, EventArgs e)
        {
            if (bw.IsBusy)
            {
                bw.CancelAsync();
            }
            if (LoopTimer.Enabled == true)
            {
                TimeTotal = 0;
                LoopIndex = 1;
                LoopTimer.Enabled = false;
            }
        }

        private void Label_Referesh_Tick(object sender, EventArgs e)
        {
            String s = "目前第" + CharacterNum.ToString() + "隻";
            NumOfCharacter.Text = s;
            NumOfCharacter.Update();
        }
        private void NineSquarePos_Click(object sender, EventArgs e)
        {
            getNineSquareLeftTop = true;
        }
        private void LoopCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (LoopCheckBox.Checked == true)
            {
                LoopTimer.Enabled = true;
                TimeTotal = 0;
            }
            else
            {
                LoopTimer.Enabled = false;
            }
        }
        private void LoopTimer_Tick(object sender, EventArgs e)
        {
            //1320 = 22min
            if (TimeTotal <= 1320 && LoopEnabled == true)
            {
                TimeTotal++;
            }
            else
            {
                if (LoopIndex != 1 && LoopIndex <= LoopCount)
                {
                    TimeTotal = 0;
                    StartButton_Click(sender, e);
                }
                else
                {
                    TimeTotal = 0;
                }
            }

            if (LoopIndex > LoopCount)
            {
                LoopEnabled = false;
                LoopTextReadFlag = false;
                LoopIndex = 1;
                bw.CancelAsync();
            }
        }
        private void TestButton_Click(object sender, EventArgs e)
        {
        }       
        private void interruptFlagBox_CheckedChanged(object sender, EventArgs e)
        {
            if (interruptFlagBox.Checked == true)
            {
                interruptFlag = true;
            }
            else
            {
                interruptFlag = false;
            }
        }
        private void TaskTraceOffBox_CheckedChanged(object sender, EventArgs e)
        {
            if (TaskTraceOffBox.Checked == true)
            {
                taskTraceFlag = true;
            }
            else
            {
                taskTraceFlag = false;
            }
        }
        #endregion     

        
    }
}

