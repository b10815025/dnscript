﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;

namespace DNScriptWFM
{
    public partial class Form1 : Form
    {
        public bool Interval(int n)
        {
            int ms = Int32.Parse(IntervalOfMillsecond.Text);
            int totalDelay = ms * n;
            Thread.Sleep(totalDelay);
            return true;
        }
        public void PixelTimer_Tick(object sender, EventArgs e)
        {
            bgr = getPixel(MousePosition);
        }
        private Color getPixel(Point position)
        {
            using (var bitmap = new Bitmap(1, 1))
            {
                using (var graphics = Graphics.FromImage(bitmap))
                {
                    graphics.CopyFromScreen(position, new Point(0, 0), new Size(1, 1));
                }
                return bitmap.GetPixel(0, 0);
            }
        }
        public bool ScanBingo(Point point)
        {
            Color p = getPixel(point);
            while (p.ToArgb() != bingo.ToArgb())
            {
                p = getPixel(point);
            }
            return true;
        }
        public bool ScanReturn(Point point, DoWorkEventArgs e)
        {
            Interval(3);
            Color p = getPixel(point);
            while (p.ToArgb() < CharacterReturn.ToArgb())
            {
                WindowsApi.SetCursorPos(0, 0);
                Interval(2);
                if (bw.CancellationPending)
                {
                    e.Cancel = true;
                    LoopTextReadFlag = false;
                    return false;
                }
                if (MousePosition != point)
                {

                    WindowsApi.SetCursorPos(point.X, point.Y);
                    Interval(4);
                    p = getPixel(point);

                }
            }

            return true;
        }
        public bool ScanPass(Point point, DoWorkEventArgs e)
        {
            Color check = getPixel(point);
            while (check.ToArgb() != mailbox.ToArgb())
            {
                WindowsApi.SetCursorPos(0, 0);
                Interval(2);
                if (bw.CancellationPending)
                {
                    e.Cancel = true;
                    LoopTextReadFlag = false;
                    return false;
                }
                if (MousePosition != point)
                {

                    WindowsApi.SetCursorPos(point.X, point.Y);
                    Interval(4);
                    check = getPixel(point);

                }

            }
            return true;

        }
    }
}
