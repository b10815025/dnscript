﻿namespace DNScriptWFM
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.StartButton = new System.Windows.Forms.Button();
            this.LoginX = new System.Windows.Forms.TextBox();
            this.LoginY = new System.Windows.Forms.TextBox();
            this.LoginConfirm = new System.Windows.Forms.Button();
            this.Character1PosX = new System.Windows.Forms.TextBox();
            this.Character1PosY = new System.Windows.Forms.TextBox();
            this.RadioPosX = new System.Windows.Forms.TextBox();
            this.RadioPosY = new System.Windows.Forms.TextBox();
            this.OnlinePosX = new System.Windows.Forms.TextBox();
            this.OnlinePosY = new System.Windows.Forms.TextBox();
            this.Character1 = new System.Windows.Forms.Button();
            this.Radio = new System.Windows.Forms.Button();
            this.Online = new System.Windows.Forms.Button();
            this.BingoPosX = new System.Windows.Forms.TextBox();
            this.BingoPosY = new System.Windows.Forms.TextBox();
            this.BingoInputPosX = new System.Windows.Forms.TextBox();
            this.AssignInputPosX = new System.Windows.Forms.TextBox();
            this.BingoInputPosY = new System.Windows.Forms.TextBox();
            this.AssignInputPosY = new System.Windows.Forms.TextBox();
            this.Bingo = new System.Windows.Forms.Button();
            this.BingoInput = new System.Windows.Forms.Button();
            this.AssignBingo = new System.Windows.Forms.Button();
            this.ReturnCharacterPosX = new System.Windows.Forms.TextBox();
            this.ReturnCharacterPosY = new System.Windows.Forms.TextBox();
            this.ReturnCharacter = new System.Windows.Forms.Button();
            this.PixelTimer = new System.Windows.Forms.Timer(this.components);
            this.interruptFlagBox = new System.Windows.Forms.CheckBox();
            this.TaskTraceOffBox = new System.Windows.Forms.CheckBox();
            this.CharacterTotalBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Stop = new System.Windows.Forms.Button();
            this.Offset = new System.Windows.Forms.TextBox();
            this.getBonus = new System.Windows.Forms.CheckBox();
            this.EventButton = new System.Windows.Forms.Button();
            this.EventButtonPosY = new System.Windows.Forms.TextBox();
            this.EventButtonPosX = new System.Windows.Forms.TextBox();
            this.MailButton = new System.Windows.Forms.Button();
            this.MailPosY = new System.Windows.Forms.TextBox();
            this.MailPosX = new System.Windows.Forms.TextBox();
            this.CharacterIndex = new System.Windows.Forms.Label();
            this.IndexOfOthers = new System.Windows.Forms.TextBox();
            this.IndexOfEventPage = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.UnDisconnectBox = new System.Windows.Forms.CheckBox();
            this.IntervalOfMillsecond = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.NumOfCharacter = new System.Windows.Forms.Label();
            this.Label_Referesh = new System.Windows.Forms.Timer(this.components);
            this.BugMode = new System.Windows.Forms.CheckBox();
            this.NumLeftTop = new System.Windows.Forms.TextBox();
            this.NumTop = new System.Windows.Forms.TextBox();
            this.NumRightTop = new System.Windows.Forms.TextBox();
            this.NumLeft = new System.Windows.Forms.TextBox();
            this.NumCenter = new System.Windows.Forms.TextBox();
            this.NumRight = new System.Windows.Forms.TextBox();
            this.NumLeftBottom = new System.Windows.Forms.TextBox();
            this.NumBottom = new System.Windows.Forms.TextBox();
            this.NumRightBottom = new System.Windows.Forms.TextBox();
            this.AutoNumChecked = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.resolutionXBox = new System.Windows.Forms.TextBox();
            this.resolutionYBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.NineSquarePos = new System.Windows.Forms.Button();
            this.NineSquarePosY = new System.Windows.Forms.TextBox();
            this.NineSquarePosX = new System.Windows.Forms.TextBox();
            this.TestChecked = new System.Windows.Forms.CheckBox();
            this.EscapeBox = new System.Windows.Forms.CheckBox();
            this.LoopTimer = new System.Windows.Forms.Timer(this.components);
            this.LoopCheckBox = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.LoopCountBox = new System.Windows.Forms.TextBox();
            this.IntervalOfNumAssign = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.TestButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // StartButton
            // 
            this.StartButton.AllowDrop = true;
            this.StartButton.Location = new System.Drawing.Point(668, 388);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(120, 50);
            this.StartButton.TabIndex = 0;
            this.StartButton.Text = "開始";
            this.StartButton.UseVisualStyleBackColor = true;
            this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // LoginX
            // 
            this.LoginX.Location = new System.Drawing.Point(28, 22);
            this.LoginX.Name = "LoginX";
            this.LoginX.Size = new System.Drawing.Size(100, 22);
            this.LoginX.TabIndex = 1;
            // 
            // LoginY
            // 
            this.LoginY.Location = new System.Drawing.Point(150, 22);
            this.LoginY.Name = "LoginY";
            this.LoginY.Size = new System.Drawing.Size(100, 22);
            this.LoginY.TabIndex = 2;
            // 
            // LoginConfirm
            // 
            this.LoginConfirm.Location = new System.Drawing.Point(295, 19);
            this.LoginConfirm.Name = "LoginConfirm";
            this.LoginConfirm.Size = new System.Drawing.Size(99, 23);
            this.LoginConfirm.TabIndex = 3;
            this.LoginConfirm.Text = "登入座標確定";
            this.LoginConfirm.UseVisualStyleBackColor = true;
            this.LoginConfirm.Click += new System.EventHandler(this.LoginConfirm_Click);
            // 
            // Character1PosX
            // 
            this.Character1PosX.Location = new System.Drawing.Point(28, 50);
            this.Character1PosX.Name = "Character1PosX";
            this.Character1PosX.Size = new System.Drawing.Size(100, 22);
            this.Character1PosX.TabIndex = 4;
            // 
            // Character1PosY
            // 
            this.Character1PosY.Location = new System.Drawing.Point(150, 49);
            this.Character1PosY.Name = "Character1PosY";
            this.Character1PosY.Size = new System.Drawing.Size(100, 22);
            this.Character1PosY.TabIndex = 5;
            // 
            // RadioPosX
            // 
            this.RadioPosX.Location = new System.Drawing.Point(28, 81);
            this.RadioPosX.Name = "RadioPosX";
            this.RadioPosX.Size = new System.Drawing.Size(100, 22);
            this.RadioPosX.TabIndex = 8;
            // 
            // RadioPosY
            // 
            this.RadioPosY.Location = new System.Drawing.Point(150, 80);
            this.RadioPosY.Name = "RadioPosY";
            this.RadioPosY.Size = new System.Drawing.Size(100, 22);
            this.RadioPosY.TabIndex = 9;
            // 
            // OnlinePosX
            // 
            this.OnlinePosX.Location = new System.Drawing.Point(28, 110);
            this.OnlinePosX.Name = "OnlinePosX";
            this.OnlinePosX.Size = new System.Drawing.Size(100, 22);
            this.OnlinePosX.TabIndex = 10;
            // 
            // OnlinePosY
            // 
            this.OnlinePosY.Location = new System.Drawing.Point(150, 109);
            this.OnlinePosY.Name = "OnlinePosY";
            this.OnlinePosY.Size = new System.Drawing.Size(100, 22);
            this.OnlinePosY.TabIndex = 11;
            // 
            // Character1
            // 
            this.Character1.Location = new System.Drawing.Point(295, 48);
            this.Character1.Name = "Character1";
            this.Character1.Size = new System.Drawing.Size(99, 23);
            this.Character1.TabIndex = 12;
            this.Character1.Text = "第一個角色座標";
            this.Character1.UseVisualStyleBackColor = true;
            this.Character1.Click += new System.EventHandler(this.Character1_Click);
            // 
            // Radio
            // 
            this.Radio.Location = new System.Drawing.Point(295, 79);
            this.Radio.Name = "Radio";
            this.Radio.Size = new System.Drawing.Size(99, 23);
            this.Radio.TabIndex = 14;
            this.Radio.Text = "頻道座標";
            this.Radio.UseVisualStyleBackColor = true;
            this.Radio.Click += new System.EventHandler(this.Radio_Click);
            // 
            // Online
            // 
            this.Online.Location = new System.Drawing.Point(295, 110);
            this.Online.Name = "Online";
            this.Online.Size = new System.Drawing.Size(99, 23);
            this.Online.TabIndex = 15;
            this.Online.Text = "上線座標";
            this.Online.UseVisualStyleBackColor = true;
            this.Online.Click += new System.EventHandler(this.Online_Click);
            // 
            // BingoPosX
            // 
            this.BingoPosX.Location = new System.Drawing.Point(28, 139);
            this.BingoPosX.Name = "BingoPosX";
            this.BingoPosX.Size = new System.Drawing.Size(100, 22);
            this.BingoPosX.TabIndex = 16;
            // 
            // BingoPosY
            // 
            this.BingoPosY.Location = new System.Drawing.Point(150, 138);
            this.BingoPosY.Name = "BingoPosY";
            this.BingoPosY.Size = new System.Drawing.Size(100, 22);
            this.BingoPosY.TabIndex = 17;
            // 
            // BingoInputPosX
            // 
            this.BingoInputPosX.Location = new System.Drawing.Point(28, 168);
            this.BingoInputPosX.Name = "BingoInputPosX";
            this.BingoInputPosX.Size = new System.Drawing.Size(100, 22);
            this.BingoInputPosX.TabIndex = 18;
            // 
            // AssignInputPosX
            // 
            this.AssignInputPosX.Location = new System.Drawing.Point(28, 197);
            this.AssignInputPosX.Name = "AssignInputPosX";
            this.AssignInputPosX.Size = new System.Drawing.Size(100, 22);
            this.AssignInputPosX.TabIndex = 19;
            // 
            // BingoInputPosY
            // 
            this.BingoInputPosY.Location = new System.Drawing.Point(150, 167);
            this.BingoInputPosY.Name = "BingoInputPosY";
            this.BingoInputPosY.Size = new System.Drawing.Size(100, 22);
            this.BingoInputPosY.TabIndex = 21;
            // 
            // AssignInputPosY
            // 
            this.AssignInputPosY.Location = new System.Drawing.Point(150, 196);
            this.AssignInputPosY.Name = "AssignInputPosY";
            this.AssignInputPosY.Size = new System.Drawing.Size(100, 22);
            this.AssignInputPosY.TabIndex = 22;
            // 
            // Bingo
            // 
            this.Bingo.Location = new System.Drawing.Point(295, 137);
            this.Bingo.Name = "Bingo";
            this.Bingo.Size = new System.Drawing.Size(99, 23);
            this.Bingo.TabIndex = 24;
            this.Bingo.Text = "右下賓果座標";
            this.Bingo.UseVisualStyleBackColor = true;
            this.Bingo.Click += new System.EventHandler(this.Bingo_Click);
            // 
            // BingoInput
            // 
            this.BingoInput.Location = new System.Drawing.Point(295, 166);
            this.BingoInput.Name = "BingoInput";
            this.BingoInput.Size = new System.Drawing.Size(99, 23);
            this.BingoInput.TabIndex = 25;
            this.BingoInput.Text = "隨機輸入座標";
            this.BingoInput.UseVisualStyleBackColor = true;
            this.BingoInput.Click += new System.EventHandler(this.BingoInput_Click);
            // 
            // AssignBingo
            // 
            this.AssignBingo.Location = new System.Drawing.Point(295, 195);
            this.AssignBingo.Name = "AssignBingo";
            this.AssignBingo.Size = new System.Drawing.Size(99, 23);
            this.AssignBingo.TabIndex = 26;
            this.AssignBingo.Text = "輸入數字座標";
            this.AssignBingo.UseVisualStyleBackColor = true;
            this.AssignBingo.Click += new System.EventHandler(this.AssignBingo_Click);
            // 
            // ReturnCharacterPosX
            // 
            this.ReturnCharacterPosX.Location = new System.Drawing.Point(28, 225);
            this.ReturnCharacterPosX.Name = "ReturnCharacterPosX";
            this.ReturnCharacterPosX.Size = new System.Drawing.Size(100, 22);
            this.ReturnCharacterPosX.TabIndex = 27;
            // 
            // ReturnCharacterPosY
            // 
            this.ReturnCharacterPosY.Location = new System.Drawing.Point(150, 224);
            this.ReturnCharacterPosY.Name = "ReturnCharacterPosY";
            this.ReturnCharacterPosY.Size = new System.Drawing.Size(100, 22);
            this.ReturnCharacterPosY.TabIndex = 28;
            // 
            // ReturnCharacter
            // 
            this.ReturnCharacter.Location = new System.Drawing.Point(295, 223);
            this.ReturnCharacter.Name = "ReturnCharacter";
            this.ReturnCharacter.Size = new System.Drawing.Size(99, 23);
            this.ReturnCharacter.TabIndex = 29;
            this.ReturnCharacter.Text = "返回角色座標";
            this.ReturnCharacter.UseVisualStyleBackColor = true;
            this.ReturnCharacter.Click += new System.EventHandler(this.ReturnCharacter_Click);
            // 
            // PixelTimer
            // 
            this.PixelTimer.Enabled = true;
            this.PixelTimer.Interval = 10;
            this.PixelTimer.Tick += new System.EventHandler(this.PixelTimer_Tick);
            // 
            // interruptFlagBox
            // 
            this.interruptFlagBox.AutoSize = true;
            this.interruptFlagBox.Location = new System.Drawing.Point(464, 25);
            this.interruptFlagBox.Name = "interruptFlagBox";
            this.interruptFlagBox.Size = new System.Drawing.Size(204, 16);
            this.interruptFlagBox.TabIndex = 30;
            this.interruptFlagBox.Text = "如果都每隻角色都輸入數字請勾起";
            this.interruptFlagBox.UseVisualStyleBackColor = true;
            this.interruptFlagBox.CheckedChanged += new System.EventHandler(this.interruptFlagBox_CheckedChanged);
            // 
            // TaskTraceOffBox
            // 
            this.TaskTraceOffBox.AutoSize = true;
            this.TaskTraceOffBox.Location = new System.Drawing.Point(464, 47);
            this.TaskTraceOffBox.Name = "TaskTraceOffBox";
            this.TaskTraceOffBox.Size = new System.Drawing.Size(226, 16);
            this.TaskTraceOffBox.TabIndex = 31;
            this.TaskTraceOffBox.Text = "如果\"關閉\"任務追蹤打勾這裡也請打勾";
            this.TaskTraceOffBox.UseVisualStyleBackColor = true;
            this.TaskTraceOffBox.CheckedChanged += new System.EventHandler(this.TaskTraceOffBox_CheckedChanged);
            // 
            // CharacterTotalBox
            // 
            this.CharacterTotalBox.Location = new System.Drawing.Point(464, 164);
            this.CharacterTotalBox.Name = "CharacterTotalBox";
            this.CharacterTotalBox.Size = new System.Drawing.Size(100, 22);
            this.CharacterTotalBox.TabIndex = 32;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(462, 149);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 33;
            this.label1.Text = "角色總數";
            // 
            // Stop
            // 
            this.Stop.Location = new System.Drawing.Point(531, 388);
            this.Stop.Name = "Stop";
            this.Stop.Size = new System.Drawing.Size(118, 50);
            this.Stop.TabIndex = 34;
            this.Stop.Text = "停止";
            this.Stop.UseVisualStyleBackColor = true;
            this.Stop.Click += new System.EventHandler(this.Stop_Click);
            // 
            // Offset
            // 
            this.Offset.Location = new System.Drawing.Point(464, 195);
            this.Offset.Name = "Offset";
            this.Offset.Size = new System.Drawing.Size(100, 22);
            this.Offset.TabIndex = 35;
            this.Offset.Text = "從第幾隻開始";
            // 
            // getBonus
            // 
            this.getBonus.AutoSize = true;
            this.getBonus.Location = new System.Drawing.Point(464, 70);
            this.getBonus.Name = "getBonus";
            this.getBonus.Size = new System.Drawing.Size(119, 16);
            this.getBonus.TabIndex = 36;
            this.getBonus.Text = "領獎(不會按bingo)";
            this.getBonus.UseVisualStyleBackColor = true;
            this.getBonus.CheckedChanged += new System.EventHandler(this.getBonus_CheckedChanged);
            // 
            // EventButton
            // 
            this.EventButton.Location = new System.Drawing.Point(295, 251);
            this.EventButton.Name = "EventButton";
            this.EventButton.Size = new System.Drawing.Size(99, 23);
            this.EventButton.TabIndex = 39;
            this.EventButton.Text = "活動賓果座標";
            this.EventButton.UseVisualStyleBackColor = true;
            this.EventButton.Click += new System.EventHandler(this.EventButton_Click);
            // 
            // EventButtonPosY
            // 
            this.EventButtonPosY.Location = new System.Drawing.Point(150, 252);
            this.EventButtonPosY.Name = "EventButtonPosY";
            this.EventButtonPosY.Size = new System.Drawing.Size(100, 22);
            this.EventButtonPosY.TabIndex = 38;
            // 
            // EventButtonPosX
            // 
            this.EventButtonPosX.Location = new System.Drawing.Point(28, 253);
            this.EventButtonPosX.Name = "EventButtonPosX";
            this.EventButtonPosX.Size = new System.Drawing.Size(100, 22);
            this.EventButtonPosX.TabIndex = 37;
            // 
            // MailButton
            // 
            this.MailButton.Location = new System.Drawing.Point(295, 279);
            this.MailButton.Name = "MailButton";
            this.MailButton.Size = new System.Drawing.Size(99, 23);
            this.MailButton.TabIndex = 42;
            this.MailButton.Text = "信箱座標";
            this.MailButton.UseVisualStyleBackColor = true;
            this.MailButton.Click += new System.EventHandler(this.MailButton_Click);
            // 
            // MailPosY
            // 
            this.MailPosY.Location = new System.Drawing.Point(150, 280);
            this.MailPosY.Name = "MailPosY";
            this.MailPosY.Size = new System.Drawing.Size(100, 22);
            this.MailPosY.TabIndex = 41;
            // 
            // MailPosX
            // 
            this.MailPosX.Location = new System.Drawing.Point(28, 281);
            this.MailPosX.Name = "MailPosX";
            this.MailPosX.Size = new System.Drawing.Size(100, 22);
            this.MailPosX.TabIndex = 40;
            // 
            // CharacterIndex
            // 
            this.CharacterIndex.AutoSize = true;
            this.CharacterIndex.Location = new System.Drawing.Point(601, 204);
            this.CharacterIndex.Name = "CharacterIndex";
            this.CharacterIndex.Size = new System.Drawing.Size(8, 12);
            this.CharacterIndex.TabIndex = 43;
            this.CharacterIndex.Text = " ";
            // 
            // IndexOfOthers
            // 
            this.IndexOfOthers.Location = new System.Drawing.Point(464, 252);
            this.IndexOfOthers.Name = "IndexOfOthers";
            this.IndexOfOthers.Size = new System.Drawing.Size(100, 22);
            this.IndexOfOthers.TabIndex = 44;
            // 
            // IndexOfEventPage
            // 
            this.IndexOfEventPage.Location = new System.Drawing.Point(571, 252);
            this.IndexOfEventPage.Name = "IndexOfEventPage";
            this.IndexOfEventPage.Size = new System.Drawing.Size(100, 22);
            this.IndexOfEventPage.TabIndex = 45;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(464, 235);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 12);
            this.label2.TabIndex = 46;
            this.label2.Text = "目錄(其他)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(569, 235);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 47;
            this.label3.Text = "活動按鍵";
            // 
            // UnDisconnectBox
            // 
            this.UnDisconnectBox.AutoSize = true;
            this.UnDisconnectBox.Location = new System.Drawing.Point(464, 93);
            this.UnDisconnectBox.Name = "UnDisconnectBox";
            this.UnDisconnectBox.Size = new System.Drawing.Size(72, 16);
            this.UnDisconnectBox.TabIndex = 48;
            this.UnDisconnectBox.Text = "防止斷線";
            this.UnDisconnectBox.UseVisualStyleBackColor = true;
            // 
            // IntervalOfMillsecond
            // 
            this.IntervalOfMillsecond.Location = new System.Drawing.Point(464, 314);
            this.IntervalOfMillsecond.Name = "IntervalOfMillsecond";
            this.IntervalOfMillsecond.Size = new System.Drawing.Size(100, 22);
            this.IntervalOfMillsecond.TabIndex = 49;
            this.IntervalOfMillsecond.Text = "250";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(464, 295);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 12);
            this.label4.TabIndex = 50;
            this.label4.Text = "動作間隔毫秒";
            // 
            // NumOfCharacter
            // 
            this.NumOfCharacter.AutoSize = true;
            this.NumOfCharacter.Location = new System.Drawing.Point(576, 200);
            this.NumOfCharacter.Name = "NumOfCharacter";
            this.NumOfCharacter.Size = new System.Drawing.Size(56, 12);
            this.NumOfCharacter.TabIndex = 51;
            this.NumOfCharacter.Text = "目前第 隻";
            // 
            // Label_Referesh
            // 
            this.Label_Referesh.Enabled = true;
            this.Label_Referesh.Interval = 500;
            this.Label_Referesh.Tick += new System.EventHandler(this.Label_Referesh_Tick);
            // 
            // BugMode
            // 
            this.BugMode.AutoSize = true;
            this.BugMode.Location = new System.Drawing.Point(464, 114);
            this.BugMode.Name = "BugMode";
            this.BugMode.Size = new System.Drawing.Size(180, 16);
            this.BugMode.TabIndex = 52;
            this.BugMode.Text = "如果遇到登入問題請打勾試試";
            this.BugMode.UseVisualStyleBackColor = true;
            // 
            // NumLeftTop
            // 
            this.NumLeftTop.Location = new System.Drawing.Point(30, 355);
            this.NumLeftTop.Name = "NumLeftTop";
            this.NumLeftTop.Size = new System.Drawing.Size(26, 22);
            this.NumLeftTop.TabIndex = 51;
            // 
            // NumTop
            // 
            this.NumTop.Location = new System.Drawing.Point(62, 355);
            this.NumTop.Name = "NumTop";
            this.NumTop.Size = new System.Drawing.Size(26, 22);
            this.NumTop.TabIndex = 52;
            // 
            // NumRightTop
            // 
            this.NumRightTop.Location = new System.Drawing.Point(94, 355);
            this.NumRightTop.Name = "NumRightTop";
            this.NumRightTop.Size = new System.Drawing.Size(26, 22);
            this.NumRightTop.TabIndex = 53;
            // 
            // NumLeft
            // 
            this.NumLeft.Location = new System.Drawing.Point(30, 383);
            this.NumLeft.Name = "NumLeft";
            this.NumLeft.Size = new System.Drawing.Size(26, 22);
            this.NumLeft.TabIndex = 54;
            // 
            // NumCenter
            // 
            this.NumCenter.Location = new System.Drawing.Point(62, 383);
            this.NumCenter.Name = "NumCenter";
            this.NumCenter.Size = new System.Drawing.Size(26, 22);
            this.NumCenter.TabIndex = 55;
            // 
            // NumRight
            // 
            this.NumRight.Location = new System.Drawing.Point(94, 383);
            this.NumRight.Name = "NumRight";
            this.NumRight.Size = new System.Drawing.Size(26, 22);
            this.NumRight.TabIndex = 56;
            // 
            // NumLeftBottom
            // 
            this.NumLeftBottom.Location = new System.Drawing.Point(30, 411);
            this.NumLeftBottom.Name = "NumLeftBottom";
            this.NumLeftBottom.Size = new System.Drawing.Size(26, 22);
            this.NumLeftBottom.TabIndex = 57;
            // 
            // NumBottom
            // 
            this.NumBottom.Location = new System.Drawing.Point(62, 411);
            this.NumBottom.Name = "NumBottom";
            this.NumBottom.Size = new System.Drawing.Size(26, 22);
            this.NumBottom.TabIndex = 58;
            // 
            // NumRightBottom
            // 
            this.NumRightBottom.Location = new System.Drawing.Point(94, 411);
            this.NumRightBottom.Name = "NumRightBottom";
            this.NumRightBottom.Size = new System.Drawing.Size(26, 22);
            this.NumRightBottom.TabIndex = 59;
            // 
            // AutoNumChecked
            // 
            this.AutoNumChecked.AutoSize = true;
            this.AutoNumChecked.Location = new System.Drawing.Point(674, 24);
            this.AutoNumChecked.Name = "AutoNumChecked";
            this.AutoNumChecked.Size = new System.Drawing.Size(72, 16);
            this.AutoNumChecked.TabIndex = 60;
            this.AutoNumChecked.Text = "自填數字";
            this.AutoNumChecked.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(137, 396);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 61;
            this.label5.Text = "遊戲解析度";
            // 
            // resolutionXBox
            // 
            this.resolutionXBox.Location = new System.Drawing.Point(140, 411);
            this.resolutionXBox.Name = "resolutionXBox";
            this.resolutionXBox.Size = new System.Drawing.Size(100, 22);
            this.resolutionXBox.TabIndex = 62;
            // 
            // resolutionYBox
            // 
            this.resolutionYBox.Location = new System.Drawing.Point(262, 411);
            this.resolutionYBox.Name = "resolutionYBox";
            this.resolutionYBox.Size = new System.Drawing.Size(100, 22);
            this.resolutionYBox.TabIndex = 63;            
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(246, 419);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 12);
            this.label7.TabIndex = 64;
            this.label7.Text = "X";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(30, 339);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 12);
            this.label6.TabIndex = 65;
            this.label6.Text = "九宮格數字";
            // 
            // NineSquarePos
            // 
            this.NineSquarePos.Location = new System.Drawing.Point(295, 307);
            this.NineSquarePos.Name = "NineSquarePos";
            this.NineSquarePos.Size = new System.Drawing.Size(99, 23);
            this.NineSquarePos.TabIndex = 68;
            this.NineSquarePos.Text = "九宮格左上座標";
            this.NineSquarePos.UseVisualStyleBackColor = true;
            this.NineSquarePos.Click += new System.EventHandler(this.NineSquarePos_Click);
            // 
            // NineSquarePosY
            // 
            this.NineSquarePosY.Location = new System.Drawing.Point(150, 308);
            this.NineSquarePosY.Name = "NineSquarePosY";
            this.NineSquarePosY.Size = new System.Drawing.Size(100, 22);
            this.NineSquarePosY.TabIndex = 67;
            // 
            // NineSquarePosX
            // 
            this.NineSquarePosX.Location = new System.Drawing.Point(28, 309);
            this.NineSquarePosX.Name = "NineSquarePosX";
            this.NineSquarePosX.Size = new System.Drawing.Size(100, 22);
            this.NineSquarePosX.TabIndex = 66;
            // 
            // TestChecked
            // 
            this.TestChecked.AutoSize = true;
            this.TestChecked.Location = new System.Drawing.Point(674, 366);
            this.TestChecked.Name = "TestChecked";
            this.TestChecked.Size = new System.Drawing.Size(87, 16);
            this.TestChecked.TabIndex = 69;
            this.TestChecked.Text = "Test Checked";
            this.TestChecked.UseVisualStyleBackColor = true;
            this.TestChecked.Visible = false;
            // 
            // EscapeBox
            // 
            this.EscapeBox.AutoSize = true;
            this.EscapeBox.Location = new System.Drawing.Point(650, 112);
            this.EscapeBox.Name = "EscapeBox";
            this.EscapeBox.Size = new System.Drawing.Size(156, 16);
            this.EscapeBox.TabIndex = 70;
            this.EscapeBox.Text = "遇到偶發無法脫離請打勾";
            this.EscapeBox.UseVisualStyleBackColor = true;
            // 
            // LoopTimer
            // 
            this.LoopTimer.Interval = 1000;
            this.LoopTimer.Tick += new System.EventHandler(this.LoopTimer_Tick);
            // 
            // LoopCheckBox
            // 
            this.LoopCheckBox.AutoSize = true;
            this.LoopCheckBox.Location = new System.Drawing.Point(650, 135);
            this.LoopCheckBox.Name = "LoopCheckBox";
            this.LoopCheckBox.Size = new System.Drawing.Size(72, 16);
            this.LoopCheckBox.TabIndex = 71;
            this.LoopCheckBox.Text = "自動周回";
            this.LoopCheckBox.UseVisualStyleBackColor = true;
            this.LoopCheckBox.CheckedChanged += new System.EventHandler(this.LoopCheckBox_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(650, 158);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 72;
            this.label8.Text = "周回次數";
            // 
            // LoopCountBox
            // 
            this.LoopCountBox.Location = new System.Drawing.Point(650, 174);
            this.LoopCountBox.Name = "LoopCountBox";
            this.LoopCountBox.Size = new System.Drawing.Size(100, 22);
            this.LoopCountBox.TabIndex = 73;
            this.LoopCountBox.Text = "1";
            // 
            // IntervalOfNumAssign
            // 
            this.IntervalOfNumAssign.Location = new System.Drawing.Point(578, 313);
            this.IntervalOfNumAssign.Name = "IntervalOfNumAssign";
            this.IntervalOfNumAssign.Size = new System.Drawing.Size(100, 22);
            this.IntervalOfNumAssign.TabIndex = 74;
            this.IntervalOfNumAssign.Text = "250";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(578, 295);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 12);
            this.label9.TabIndex = 75;
            this.label9.Text = "填數字間隔";
            // 
            // TestButton
            // 
            this.TestButton.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.TestButton.Location = new System.Drawing.Point(466, 353);
            this.TestButton.Name = "TestButton";
            this.TestButton.Size = new System.Drawing.Size(75, 23);
            this.TestButton.TabIndex = 76;
            this.TestButton.Text = "button1";
            this.TestButton.UseVisualStyleBackColor = true;
            this.TestButton.Visible = false;
            this.TestButton.Click += new System.EventHandler(this.TestButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.TestButton);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.IntervalOfNumAssign);
            this.Controls.Add(this.LoopCountBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.LoopCheckBox);
            this.Controls.Add(this.EscapeBox);
            this.Controls.Add(this.TestChecked);
            this.Controls.Add(this.NineSquarePos);
            this.Controls.Add(this.NineSquarePosY);
            this.Controls.Add(this.NineSquarePosX);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.BugMode);
            this.Controls.Add(this.NumOfCharacter);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.IntervalOfMillsecond);
            this.Controls.Add(this.UnDisconnectBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.IndexOfEventPage);
            this.Controls.Add(this.IndexOfOthers);
            this.Controls.Add(this.CharacterIndex);
            this.Controls.Add(this.MailButton);
            this.Controls.Add(this.MailPosY);
            this.Controls.Add(this.MailPosX);
            this.Controls.Add(this.EventButton);
            this.Controls.Add(this.EventButtonPosY);
            this.Controls.Add(this.EventButtonPosX);
            this.Controls.Add(this.getBonus);
            this.Controls.Add(this.Offset);
            this.Controls.Add(this.Stop);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CharacterTotalBox);
            this.Controls.Add(this.TaskTraceOffBox);
            this.Controls.Add(this.interruptFlagBox);
            this.Controls.Add(this.ReturnCharacter);
            this.Controls.Add(this.ReturnCharacterPosY);
            this.Controls.Add(this.ReturnCharacterPosX);
            this.Controls.Add(this.AssignBingo);
            this.Controls.Add(this.BingoInput);
            this.Controls.Add(this.Bingo);
            this.Controls.Add(this.AssignInputPosY);
            this.Controls.Add(this.BingoInputPosY);
            this.Controls.Add(this.AssignInputPosX);
            this.Controls.Add(this.BingoInputPosX);
            this.Controls.Add(this.BingoPosY);
            this.Controls.Add(this.BingoPosX);
            this.Controls.Add(this.Online);
            this.Controls.Add(this.Radio);
            this.Controls.Add(this.Character1);
            this.Controls.Add(this.OnlinePosY);
            this.Controls.Add(this.OnlinePosX);
            this.Controls.Add(this.RadioPosY);
            this.Controls.Add(this.RadioPosX);
            this.Controls.Add(this.Character1PosY);
            this.Controls.Add(this.Character1PosX);
            this.Controls.Add(this.LoginConfirm);
            this.Controls.Add(this.LoginY);
            this.Controls.Add(this.LoginX);
            this.Controls.Add(this.StartButton);
            this.Controls.Add(this.resolutionYBox);
            this.Controls.Add(this.resolutionXBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.AutoNumChecked);
            this.Controls.Add(this.NumRightBottom);
            this.Controls.Add(this.NumBottom);
            this.Controls.Add(this.NumLeftBottom);
            this.Controls.Add(this.NumRight);
            this.Controls.Add(this.NumCenter);
            this.Controls.Add(this.NumLeft);
            this.Controls.Add(this.NumRightTop);
            this.Controls.Add(this.NumTop);
            this.Controls.Add(this.NumLeftTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "Form1";
            this.Text = "DNScript Ver3.0";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form1_KeyPress);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.TextBox LoginX;
        private System.Windows.Forms.TextBox LoginY;
        private System.Windows.Forms.Button LoginConfirm;
        private System.Windows.Forms.TextBox Character1PosX;
        private System.Windows.Forms.TextBox Character1PosY;
        private System.Windows.Forms.TextBox RadioPosX;
        private System.Windows.Forms.TextBox RadioPosY;
        private System.Windows.Forms.TextBox OnlinePosX;
        private System.Windows.Forms.TextBox OnlinePosY;
        private System.Windows.Forms.Button Character1;
        private System.Windows.Forms.Button Radio;
        private System.Windows.Forms.Button Online;
        private System.Windows.Forms.TextBox BingoPosX;
        private System.Windows.Forms.TextBox BingoPosY;
        private System.Windows.Forms.TextBox BingoInputPosX;
        private System.Windows.Forms.TextBox AssignInputPosX;
        private System.Windows.Forms.TextBox BingoInputPosY;
        private System.Windows.Forms.TextBox AssignInputPosY;
        private System.Windows.Forms.Button Bingo;
        private System.Windows.Forms.Button BingoInput;
        private System.Windows.Forms.Button AssignBingo;
        private System.Windows.Forms.TextBox ReturnCharacterPosX;
        private System.Windows.Forms.TextBox ReturnCharacterPosY;
        private System.Windows.Forms.Button ReturnCharacter;       
        private System.Windows.Forms.Timer PixelTimer;
        private System.Windows.Forms.CheckBox interruptFlagBox;
        private System.Windows.Forms.CheckBox TaskTraceOffBox;
        private System.Windows.Forms.TextBox CharacterTotalBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Stop;
        private System.Windows.Forms.TextBox Offset;
        private System.Windows.Forms.CheckBox getBonus;
        private System.Windows.Forms.Button EventButton;
        private System.Windows.Forms.TextBox EventButtonPosY;
        private System.Windows.Forms.TextBox EventButtonPosX;
        private System.Windows.Forms.Button MailButton;
        private System.Windows.Forms.TextBox MailPosY;
        private System.Windows.Forms.TextBox MailPosX;
        private System.Windows.Forms.Label CharacterIndex;
        private System.Windows.Forms.TextBox IndexOfOthers;
        private System.Windows.Forms.TextBox IndexOfEventPage;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox UnDisconnectBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label NumOfCharacter;
        private System.Windows.Forms.Timer Label_Referesh;
        private System.Windows.Forms.CheckBox BugMode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button NineSquarePos;
        private System.Windows.Forms.TextBox NineSquarePosY;
        private System.Windows.Forms.TextBox NineSquarePosX;
        private System.Windows.Forms.CheckBox TestChecked;
        public System.Windows.Forms.TextBox IntervalOfMillsecond;
        public System.Windows.Forms.TextBox NumLeftTop;
        public System.Windows.Forms.TextBox NumTop;
        public System.Windows.Forms.TextBox NumRightTop;
        public System.Windows.Forms.TextBox NumLeft;
        public System.Windows.Forms.TextBox NumCenter;
        public System.Windows.Forms.TextBox NumRight;
        public System.Windows.Forms.TextBox NumLeftBottom;
        public System.Windows.Forms.TextBox NumBottom;
        public System.Windows.Forms.TextBox NumRightBottom;
        public System.Windows.Forms.CheckBox AutoNumChecked;
        public System.Windows.Forms.TextBox resolutionXBox;
        public System.Windows.Forms.TextBox resolutionYBox;
        private System.Windows.Forms.CheckBox EscapeBox;
        private System.Windows.Forms.Timer LoopTimer;
        private System.Windows.Forms.CheckBox LoopCheckBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox LoopCountBox;
        private System.Windows.Forms.TextBox IntervalOfNumAssign;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button TestButton;
    }
}

