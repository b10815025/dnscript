﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;


namespace DNScriptWFM
{
    public partial class Form1 : Form
    {
        #region Boolean
        bool getLoginPos = false;
        bool getCharacter1Pos = false;
        bool getRadioPos = false;
        bool getOnlinePos = false;
        bool getBingoPos = false;
        bool getBingoInputPos = false;
        bool getAssignInputPos = false;
        bool getReturnCharacterPos = false;
        bool interruptFlag = false;
        bool taskTraceFlag = false;
        bool getGiftFlag = false;
        bool getEventPos = false;
        bool getMailPos = false;
        bool getNineSquareLeftTop = false;
        bool LoopTextReadFlag = false;
        bool LoopEnabled = false;
        #endregion
        #region Integer
        int LoopCount = 1;
        int LoopIndex = 1;
        int CharacterNum = 1;
        int CharacterTotal = 0;
        int TimeTotal = 0;
        #endregion
        #region Color
        Color bgr;
        Color bingo;
        Color mailbox;
        Color CharacterReturn;
        #endregion
        #region PointArr
        Point[] SquareDirection = new Point[9];
        #endregion
        #region BackgroundWorker_thread
        BackgroundWorker bw = new BackgroundWorker();
        #endregion
    }
}
